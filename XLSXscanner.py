
import requests
import xlrd
from openpyxl.workbook import Workbook
from openpyxl.reader.excel import load_workbook, InvalidFileException

def cvt_xls_to_xlsx(url):

    downloaded = download(url)
    newName = downloaded['name'].split('.')[0]
    book_xls = xlrd.open_workbook(downloaded['path'])
    book_xlsx = Workbook()

    sheet_names = book_xls.sheet_names()
    for sheet_index in range(0,len(sheet_names)):
        sheet_xls = book_xls.sheet_by_name(sheet_names[sheet_index])
        if sheet_index == 0:
            sheet_xlsx = book_xlsx.active
            sheet_xlsx.title = sheet_names[sheet_index]
        else:
            sheet_xlsx = book_xlsx.create_sheet(title=sheet_names[sheet_index])

        for row in range(0, sheet_xls.nrows):
            for col in range(0, sheet_xls.ncols):
                sheet_xlsx.cell(row = row+1 , column = col+1).value = sheet_xls.cell_value(row, col)
    newLocation = './converted/'+newName+'.xlsx'
    book_xlsx.save(newLocation)
    return {
      "path":newLocation,
      "name":newName
    }


def download(url):
  myfile = requests.get(url)
  fileName = url.split('/')[-1]
  open('./downloaded/'+fileName,'wb').write(myfile.content)
  return {
    "path":'./downloaded/'+fileName,
    "name":fileName
  }

testurl='https://www1.nyc.gov/assets/tlc/downloads/datasets/nys_dmv_revoked_suspended_registrations.xls'
# download('https://storage.googleapis.com/convertedexcelfiles/nys_dmv_revoked_suspended_registrations.xlsx')

def cleanseExcel(url):
  
  converted = cvt_xls_to_xlsx(url)
  print('converted',converted)
  wb=load_workbook(filename = converted['path'])
  ws=wb.active
  for row in ws:
    for cell in row:
      if cell.value is None:
        print('found none',cell)
        cell.value=""
        continue
      cell.value=cell.value.strip()
  wb.save('./cleansed/'+converted['name']+'(cleansed).xlsx')



cleanseExcel(testurl)

  #ws.delete_rows(row)

  # newFilePathArray=filePath.split('.')
  # newFilePathArray[0]+='(deleted_row_#%d)' % (row)
  # newFilePath='.'.join(newFilePathArray)
  #wb.save(destinationPath)
 
    

# deleteRow('https://storage.googleapis.com/convertedexcelfiles/nys_dmv_revoked_suspended_registrations.xlsx','/Users/Eric/Downloads/nys(updated)')